/// <reference types="cypress" />

declare namespace Cypress {
    interface Chainable<Subject = any> {
        /**
         * Executes a login.
         * @example login('admin', 'publish')
         */
        login(username: string, password: string): Chainable<any>
    }
}

/// <reference types="cypress" />

//Login to the admin backend and start the Session.
Cypress.Commands.add('login', (username: string, password: string) => {
    cy.session(
        [username, password],
        () => {
            cy.visit("/admin/login");
            cy.get("#username").should("have.length", 1);
            cy.get("#password").should("have.length", 1);
            cy.get("#username").type(username);
            cy.get("#password").type(password);
            cy.get('form').submit();
            cy.url().should('contain', '/admin/dashboard');
        },
        {
            validate() {
                cy.request('/admin/user/settings/list').its('status').should('eq', 200);
            }
        }
    )
});
// Convert this to a module instead of script (allows import/export)
export {}